import * as React from 'react'
import { BrowserRouter, Route, Switch, HashRouter, Redirect } from 'react-router-dom'
import LayoutComponent from '../components/layoutComponent';
import LoginContainer from '../components/loginContainer';
import NotFoundComponent from '../components/notFoundComponent';
import CountrySelectionContainer from '../components/countrySelectionContainer';
import OrdersTableContainer from '../components/ordersTableContainer';
import Layout from '../components/layoutComponent'
import { ApplicationState } from '../store/applicationState';
import { connect } from 'react-redux';
import { Select } from 'antd';
import OrderDetailsContainer from '../components/orderDetailsContainer';


interface RoutesProps {
    isLoggedIn: boolean
    selectedCountryCode: string
}

class Routes extends React.Component<RoutesProps, {}>{

    constructor(props: RoutesProps) {
        super(props);
    }

    render() {
        return (
            <HashRouter>
                <Layout>
                    <Switch>
                        <Route path="/countrySelection" component={CountrySelectionContainer} />
                        <Route path="/orders/details/:orderId" component={OrderDetailsContainer} />
                        <Route path="/orders/:countryCode" component={OrdersTableContainer} />
                        <Route path="/login" component={LoginContainer} />
                        
                        <Route path="/" exact render={() => {
                            if (this.props.isLoggedIn) {
                                if (this.props.selectedCountryCode !== "") {
                                    return <Redirect to={"/orders/" + this.props.selectedCountryCode} />
                                }
                                else {
                                    return <Redirect to={"/countrySelection"}/>
                                }
                            }
                            
                            if (!this.props.isLoggedIn) {
                                return <Redirect to="/login"/>
                            }
                            return null;
                        }} />
                        <Route path="*" component={NotFoundComponent} />
                    </Switch>

                </Layout>
            </HashRouter>);
    }

}

const mapStateToProps = (state: ApplicationState): RoutesProps => {
    return {
        isLoggedIn: state.authorization.token != null,
        selectedCountryCode: state.countryState.selectedCountryCode
    }
}


export default connect(mapStateToProps)(Routes);
