import * as React from "react";
import { connect } from 'react-redux'
import NavBar from "./navBarComponent";

import { ApplicationState } from "../store/applicationState";
import { Country } from "../store/state/countryState";
import { OrderDetails } from "./orderDetailsComponent";
import { Customer, Restaurant } from "../store/state/orderState";

interface OrderDetailsContainerProps {
    customerDetails: Customer,
    restaurantDetails: Restaurant
}

class OrderDetailsContainer extends React.Component<OrderDetailsContainerProps, {}> {

    constructor(props: any) {
        super(props);
    }

    render() {
        return (
            <OrderDetails
                customerDetails={this.props.customerDetails}
                restaurantDetails={this.props.restaurantDetails}/>
        );
    }
}

const mapStateToProps = (state: ApplicationState): OrderDetailsContainerProps => {

    const selectedOrder = state.orderState.orders
        .filter(x => x.orderId == state.orderState.selectedOrderId)[0];

    return {
        customerDetails : selectedOrder.customerName,
        restaurantDetails : selectedOrder.restaurant
    }
}

export default connect(mapStateToProps)(OrderDetailsContainer);