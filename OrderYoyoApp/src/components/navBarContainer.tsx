import * as React from "react";
import { connect } from 'react-redux'
import NavBar from "./navBarComponent";

import { ApplicationState } from "../store/applicationState";
import { Country } from "../store/state/countryState";

interface NavBarContainerProps {
    isLoggedIn: boolean,
    countries: Array<Country>,
	selectedCountry?: Country
}

class NavBarContainer extends React.Component<NavBarContainerProps, {}> {

    constructor(props: any) {
        super(props);
    }

    render() {
        return (
            <NavBar
                countries={this.props.countries}
                isLoggedIn={this.props.isLoggedIn}
                selectedCountry={this.props.selectedCountry}
            />
        );
    }
}

const mapStateToProps = (state: ApplicationState): NavBarContainerProps => {

    const selectedCountry = state.countryState.countries.filter(x => x.code == state.countryState.selectedCountryCode)[0];

    const filterdCountries = selectedCountry != null
        ? state.countryState.countries.filter(x => x.code != selectedCountry.code)
        : state.countryState.countries

    return {
        isLoggedIn: state.authorization.token != null,
        countries: filterdCountries,
        selectedCountry: selectedCountry
    }
}

export default connect(mapStateToProps)(NavBarContainer);