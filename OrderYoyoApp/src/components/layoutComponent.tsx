import * as React from "react";
import { connect } from 'react-redux'
import { Layout, Menu, Icon } from 'antd'

import 'antd/dist/antd.css'
import NavBarContainer from "./navBarContainer";

const { Header, Content, Footer } = Layout;

class LayoutComponent extends React.Component<{}, {}> {

    constructor(props: any) {
        super(props);
    }

    render() {
        return (
            <Layout>
                <NavBarContainer />
                <Content style={{ padding: '0 50px' }}>
                    {this.props.children}
                </Content>
                <Footer style={{ textAlign: 'center' }}>
                    Order YoYo sample app @2018 Created by Tomasz Wegiel
                </Footer>
            </Layout>
        );
    }
}

export default LayoutComponent