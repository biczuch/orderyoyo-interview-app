import * as React from 'react'
import { connect } from 'react-redux'
import { ApplicationState } from '../store/applicationState'

import { withRouter, Redirect } from 'react-router-dom';
import { CountrySelection } from './countrySelectionComponent';
import { Country } from '../store/state/countryState';
import { actionCreators, CountryThunkAction } from '../store/actions/countryActions';
import { ThunkAction } from 'redux-thunk';

interface CountrySelectionProps {
    countries: Array<Country>,
    isLoading: boolean
}

interface ActionProps {
    requestCountries: () => CountryThunkAction<void>
    selectCountry: (countryCode: string) => CountryThunkAction<void>
}

type CountrySelectionAllProps = CountrySelectionProps & ActionProps

class CountrySelectionContainer extends React.Component<CountrySelectionAllProps, {}>{

    constructor(props: CountrySelectionAllProps) {
        super(props);
    }

    componentDidMount() {
        if (this.props.countries.length == 0) {
            this.props.requestCountries();
        }
    }

    render() {
        return (
            <CountrySelection
                countries={this.props.countries}
                isLoading={this.props.isLoading}
                countrySelected={this.props.selectCountry}
            />
        );
    }
}

const mapStateToProps = (state: ApplicationState): CountrySelectionProps => {
    return {
        countries: state.countryState.countries,
        isLoading: state.countryState.isLoading
    }
}

const mapDispatchToProps = function (): ActionProps {
    return {
        requestCountries: actionCreators.requestCountries,
        selectCountry: actionCreators.selectCountry
    }
}

export default connect(mapStateToProps, mapDispatchToProps())(CountrySelectionContainer);