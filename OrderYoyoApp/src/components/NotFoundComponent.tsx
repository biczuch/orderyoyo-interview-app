import * as React from 'react'


class NotFound extends React.Component<{}, {}>{

    constructor(props: any) {
        super(props);
    }

    render() {
        return <h3>Error! Path does not exist.</h3>
    }
}


export default NotFound;