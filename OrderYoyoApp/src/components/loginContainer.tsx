import * as React from 'react'
import { connect } from 'react-redux'
import { LoginForm, LoginFormFields } from './loginComponent'
import { ApplicationState } from '../store/applicationState'

import { actionCreators, LoginThunkAction } from '../store/actions/loginActions'
import { SimpleFormState } from '../store/state/formState';
import { withRouter, Redirect } from 'react-router-dom';

interface LoginContainerProps {
    loginFormState: SimpleFormState
}

interface ActionProps {
    requestLogin: (username: string, password: string, rememberMe: boolean) => LoginThunkAction<void>
}

type AddOrderContainerAllProps = LoginContainerProps & ActionProps

class LoginContainer extends React.Component<AddOrderContainerAllProps, {}>{

    constructor(props: AddOrderContainerAllProps) {
        super(props);
        this.submitForm = this.submitForm.bind(this);
    }

    submitForm(formValues: LoginFormFields) {
        this.props.requestLogin(
            formValues.username,
            formValues.password,
            formValues.rememberCredentials);
    }

    render() {
        if (this.props.loginFormState.isSuccessful) {
            return (
                <Redirect to="/" />
            );
        }
        return (
            <LoginForm handleSubmit={this.submitForm} formErrors={this.props.loginFormState} />
        );


    }
}

const mapStateToProps = (state: ApplicationState): LoginContainerProps => {
    return {
        loginFormState: state.formsErrorState.loginForm
    }
}

const mapDispatchToProps = function (): ActionProps {
    return {
        requestLogin: actionCreators.requestLogin
    }
}

export default connect(mapStateToProps, mapDispatchToProps())(LoginContainer);