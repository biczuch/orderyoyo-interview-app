import * as React from 'react'
import { Menu, Icon, Select } from 'antd';
import { Link, NavLink } from 'react-router-dom';
import { Country } from '../store/state/countryState';
import countryReducer from '../store/reducers/countryReducer';

const SubMenu = Menu.SubMenu;
const MenuItemGroup = Menu.ItemGroup;

interface MenuProps {
    isLoggedIn: boolean,
    countries: Array<Country>,
    selectedCountry?: Country
}

class NavBar extends React.Component<MenuProps, {}>{

    constructor(props: MenuProps) {
        super(props);
        this.renderLoggedInBar = this.renderLoggedInBar.bind(this);
        this.renderNotLoggedInBar = this.renderNotLoggedInBar.bind(this);
    }

    renderNotLoggedInBar() {
        return (
            <Menu mode="horizontal">
                <Menu.Item>
                    <NavLink to="/"><Icon type="home" />Home</NavLink>
                </Menu.Item>
                <Menu.Item>
                    <NavLink to="/login"><Icon type="login" />Log in</NavLink>
                </Menu.Item>
            </Menu>
        );
    }

    renderLoggedInBar() {

        const selectedCountry = this.props.selectedCountry;

        const subMenuTitle = selectedCountry != null
            ? selectedCountry.name
            : "Countries"

        return (
            <Menu mode="horizontal">
                <Menu.Item>
                    <NavLink to="/"><Icon type="home" />Home</NavLink>
                </Menu.Item>
                <Menu.Item>
                    <NavLink to="/countrySelection"><Icon type="profile" />{subMenuTitle}</NavLink>
                </Menu.Item>
            </Menu>
        );
    }

    render() {
        if (this.props.isLoggedIn) {
            return this.renderLoggedInBar();
        }
        else {
            return this.renderNotLoggedInBar();
        }
    }

}

export default NavBar