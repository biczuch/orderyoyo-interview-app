import * as React from 'react'
import { Component } from 'react';
import { CountryState, Country } from '../store/state/countryState';
import { List, Row, Col } from 'antd';
import { Link } from 'react-router-dom';


export interface CountrySelectionProps {
    countries: Array<Country>,
    isLoading: boolean
    countrySelected: (countryCode: string) => void
}


export class CountrySelection extends Component<CountrySelectionProps, {}> {

    render() {
        return (
            <Row>
                <Col offset={8} span={8}>
                    <h3>Select country:</h3>
                    <List loading={this.props.isLoading}
                        size="large"
                        bordered
                        dataSource={this.props.countries}
                        renderItem={
                            (item: Country) => (
                                <List.Item>
                                    <Link
                                        onClick={() => this.props.countrySelected(item.code)}
                                        to={"/orders/" + item.code}>{item.name}
                                    </Link>
                                </List.Item>)
                        }
                    />
                </Col>
            </Row>
        );
    }

}