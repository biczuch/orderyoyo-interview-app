import * as React from 'react'
import { connect } from 'react-redux'
import { ApplicationState } from '../store/applicationState'

import { withRouter, Redirect, RouteProps, RouteComponentProps } from 'react-router-dom';
import { ThunkAction } from 'redux-thunk';
import { OrdersTable, OrderTableItem, RestaurantStatus } from './ordersTableComponent';
import { OrderThunkAction, actionCreators } from '../store/actions/orderActions';
import { Order } from '../store/state/orderState';
import * as moment from 'moment'

interface OrdersContainerState {
    rowClicked: boolean
}

interface OrdersContainerProps {
    orders: Array<OrderTableItem>,
    isLoading: boolean,
    selectedOrderId: number
}

interface ActionProps {
    requestOrders: (countryCode: string) => OrderThunkAction<void>
    orderDetailsSelected: (orderId: number) => OrderThunkAction<void>
}

type OrdersContainerAllProps = OrdersContainerProps & ActionProps & RouteComponentProps<{ countryCode: string }>

class OrdersTableContainer extends React.Component<OrdersContainerAllProps, OrdersContainerState>{

    constructor(props: OrdersContainerAllProps) {
        super(props);
        this.state = {
            rowClicked: false
        }
        this.onRowClick = this.onRowClick.bind(this);
    }


    componentDidMount() {
        const countryCode = this.props.match.params.countryCode
        this.props.requestOrders(countryCode);
    }

    onRowClick(record: OrderTableItem) {
        this.setState({ rowClicked: true });
        this.props.orderDetailsSelected(record.orderId);
    }

    render() {
        if (this.state.rowClicked && this.props.selectedOrderId != 0) {
            return <Redirect to={"/orders/details/" + this.props.selectedOrderId} />
        }

        return (
            <OrdersTable
                orders={this.props.orders}
                isLoading={this.props.isLoading}
                onRowClick={this.onRowClick} />
        );
    }
}

const getDateFromTimePartString = (time: string) => {
    const timeParts = time.split(":").map(x => Number(x))
    return moment().hour(timeParts[0]).minutes(timeParts[1]);
}

const getRestaurantStatus = (order: Order): RestaurantStatus => {

    const currentDateOfWeek = moment().weekday();

    const restaurantStatusForDayOfWeek = order.restaurant.openTime
        .filter(x => x.dayOfWeek === currentDateOfWeek)[0];

    if (restaurantStatusForDayOfWeek.isClosed)
        return RestaurantStatus.closedAllDay;

    const currentTime = moment();
    const openTime = getDateFromTimePartString(restaurantStatusForDayOfWeek.openHoursFrom);
    const closeTime = getDateFromTimePartString(restaurantStatusForDayOfWeek.openHoursTo);
    const fifteenMinutesBeforeOpenTime = moment(openTime).subtract(15, "minutes");

    const isClosed = currentTime.isAfter(closeTime) || currentTime.isBefore(openTime);
    const isOpened = currentTime.isAfter(openTime) && currentTime.isBefore(closeTime);
    const isOpenedSoon = currentTime.isBefore(openTime) && currentTime.isAfter(fifteenMinutesBeforeOpenTime);

    if (isOpened)
        return RestaurantStatus.openedNow;

    if (isClosed) {
        return RestaurantStatus.notOpenedSoon
    }

    if (isOpenedSoon) {
        return RestaurantStatus.openedSoon;
    }
}

const byTimeOfOrder = (a: OrderTableItem, b: OrderTableItem) => {
    if (a.timeOfOrder < b.timeOfOrder) {
        return -1;
    }
    else if (a.timeOfOrder > b.timeOfOrder) {
        return 1;
    }
    return 0;
}

const mapToOrderTableItem = (x: Order): OrderTableItem => {
    return {
        orderId: x.orderId,
        restaurantName: x.restaurant.name,
        timeOfOrder: x.created,
        openFrom: x.restaurant.openTime.filter(x => x.dayOfWeek === new Date().getDay())[0].openHoursFrom,
        status: getRestaurantStatus(x)
    }
}

const currentYear = (x: OrderTableItem) => moment(x.timeOfOrder).year() >= moment().year()

const mapStateToProps = (state: ApplicationState): OrdersContainerProps => {
    return {
        orders: state.orderState.orders
            .map(mapToOrderTableItem)
            .filter(currentYear)
            .sort(byTimeOfOrder),
        isLoading: state.orderState.isLoading,
        selectedOrderId: state.orderState.selectedOrderId
    }
}

const mapDispatchToProps = function (): ActionProps {
    return {
        requestOrders: actionCreators.requestOrders,
        orderDetailsSelected: actionCreators.orderDetailsSelected
    }
}

export default connect(mapStateToProps, mapDispatchToProps())(OrdersTableContainer);