import * as React from 'react'
import * as ReactDom from 'react-dom'
import * as yup from 'yup'
import { FormikProps, FormikValues, withFormik, FormikActions } from 'formik'
import { Form, Input, Col, Row, Checkbox, Layout, Button } from 'antd'
import { tailFormItemLayout, nameOfField, FormProps } from '../common/formCommon';
import FormHeader from '../commonComponents/formHeader';
import { SimpleFormState } from '../store/state/formState';
import FormErrors from '../commonComponents/formError';

export interface LoginFormFields {
    username: string,
    password: string,
    rememberCredentials: boolean
}

const validationSchema = yup.object<LoginFormFields>().shape({
    username: yup.string().required().label("Username"),
    password: yup.string().required().label("Password"),
    rememberCredentials: yup.boolean().label("Remember me")
})

type LoginFormValues = LoginFormFields & FormikValues

interface LoginFormExternalProps {
    handleSubmit: (values: LoginFormValues) => void,
    formErrors: SimpleFormState
}

type LoginFormProps = FormikProps<LoginFormValues> & LoginFormExternalProps

class LoginFormController extends React.Component<LoginFormProps, {}>
{
    constructor(props: LoginFormProps) {
        super(props);
    }

    render() {
        const { isSubmitting } = this.props;
        const FormItem = Form.Item;
        const { inputProps, formItemProps, checkboxProps } = FormProps(this.props, validationSchema)
        
        return (
            <Form layout="horizontal" onSubmit={this.props.handleSubmit}>
                <FormErrors formErrors={this.props.formErrors} />
                <FormHeader title="Log in" />
                <FormItem {...formItemProps("username")}>
                    <Input type="text" {...inputProps("username")} />
                </FormItem>
                <FormItem {...formItemProps("password")}>
                    <Input type="password" {...inputProps("password")}/>
                </FormItem>
                <FormItem {...tailFormItemLayout}>
                    <Checkbox {...checkboxProps("rememberCredentials")}></Checkbox>
                </FormItem>
                <FormItem {...tailFormItemLayout}>
                    <Button type="primary" htmlType="submit" loading={isSubmitting} disabled={isSubmitting} >
                        Log in
                    </Button>
                </FormItem>
            </Form>
        );

    }
}

const mapPropsToValues = (props: LoginFormProps): LoginFormValues => {
    return {
        username: '',
        password: '',
        rememberCredentials: false
    }
}

const handleSubmit = (values: LoginFormValues, formikBag: { props: LoginFormProps } & FormikActions<LoginFormValues>): void => {
    formikBag.setSubmitting(false);
    formikBag.props.handleSubmit(values);
}

export const LoginForm = withFormik<LoginFormExternalProps, LoginFormValues>({
    mapPropsToValues,
    handleSubmit,
    validationSchema
})(LoginFormController)