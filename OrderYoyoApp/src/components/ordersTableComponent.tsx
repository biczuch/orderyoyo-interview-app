import * as React from 'react'
import { Table } from 'antd';
import Column from 'antd/lib/table/Column';
import * as moment from 'moment';
import { OrderState } from '../store/state/orderState';

import '../styles/ordersTable.css'

export enum RestaurantStatus {
    notOpenedSoon,
    openedSoon,
    openedNow,
    closedAllDay
}

export interface OrderTableItem {
    orderId: number,
    restaurantName: string,
    openFrom: string,
    timeOfOrder: Date,
    status: RestaurantStatus
}


const columns = [
    {
        title: 'Order ID',
        dataIndex: 'orderId'
    },
    {
        title: 'Restaurant Name',
        dataIndex: 'restaurantName',
    },
    {
        title: 'Open from',
        dataIndex: 'openFrom',
    },
    {
        title: 'Time of the Order',
        dataIndex: 'timeOfOrder',
        render: (text: string, record: OrderTableItem, index: number) => {
            const timeOfOrder = moment(record.timeOfOrder);
            const formattedDate = timeOfOrder.format("DD.MM.YYYY HH:MM");
            return formattedDate
        }
    }
]


interface OrdersComponentProps {
    orders: Array<OrderTableItem>,
    isLoading: boolean,
    onRowClick: (orderTableItem: OrderTableItem) => void
}



export class OrdersTable extends React.Component<OrdersComponentProps, {}>{

    constructor(props: OrdersComponentProps) {
        super(props);
    }

    addClassToRow(record: OrderTableItem, index: number): string {
        switch (record.status) {
            case RestaurantStatus.closedAllDay:
                return "restaurantClosedAllDay"
            case RestaurantStatus.notOpenedSoon:
                return "restaurantNotOpenedSoon"
            case RestaurantStatus.openedNow:
                return "restaurantOpenedNow"
            case RestaurantStatus.openedSoon:
                return "restaurantOpenedSoon"
        }
    }

    render() {
        return <Table
            dataSource={this.props.orders}
            columns={columns}
            rowKey="orderId"
            rowClassName={this.addClassToRow}
            onRow={(record) => {
                return {
                    onClick: () => this.props.onRowClick(record)
                }
            }}
        />
    }

}