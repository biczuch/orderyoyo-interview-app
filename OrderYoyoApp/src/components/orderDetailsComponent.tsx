import * as React from 'react'
import { Order, Customer, Restaurant } from '../store/state/orderState';
import { Row, Col, Card } from 'antd';

interface OrderDetailsProps {
    customerDetails: Customer,
    restaurantDetails: Restaurant
}

export class OrderDetails extends React.Component<OrderDetailsProps, {}>{

    constructor(props: OrderDetailsProps) {
        super(props);
    }

    render() {
        return (
            <Row>
                <Col span={6} offset={6}>
                    <Card title="Restaurant information">
                        <p>name: {this.props.restaurantDetails.name}</p>
                        <p>phone number: {this.props.restaurantDetails.phone}</p>
                        <p>e-mail: {this.props.restaurantDetails.email}</p>
                    </Card>
                </Col>
                <Col span={6}>
                    <Card title="User information">
                        <p>name: {this.props.customerDetails.name}</p>
                        <p>phone number: {this.props.customerDetails.phone}</p>
                        <p>physical address: {this.props.customerDetails.phone}</p>
                        <p>e-mail: {this.props.customerDetails.email}</p>
                    </Card>
                </Col>
            </Row>
        );
    }
}