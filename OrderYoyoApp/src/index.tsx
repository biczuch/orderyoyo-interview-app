import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from 'react-redux'
import App from "./App";
import { store, history } from './store/store'
import { ConnectedRouter } from "connected-react-router";
import { Route, Switch, HashRouter } from "react-router-dom";

const rootElement = document.getElementById("root");

ReactDOM.render(
    <App/>
    , rootElement
);

if ((module as any).hot) {
    (module as any).hot.accept(App, function () {
        var NextApp = App
        ReactDOM.render(<NextApp />, rootElement)
    })
}