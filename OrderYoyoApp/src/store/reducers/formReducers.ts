import { initialState } from '../applicationState'
import { AnyAction } from 'redux';
import { KnownActions, LoginActionTypes } from '../actions/loginActions'
import { FormsState } from '../state/formState';
import { history } from '../store'

export default function formsReducer(state: FormsState = initialState.formsErrorState, action: KnownActions): FormsState {
    switch (action.type) {
        case LoginActionTypes.LOGIN_REQUESTED:
            return state;
        case LoginActionTypes.LOGIN_SUCESSFUL:
            {
                const newState = {
                    loginForm: {
                        errors: [] as Array<string>,
                        hasError: false,
                        isSuccessful: true
                    }
                }
                return { ...state, ...newState }
            }
        case LoginActionTypes.LOGIN_FAILURE:
            {
                const newState = {
                    loginForm: {
                        errors: action.errors,
                        hasError: true,
                        isSuccessful: false
                    }
                }
                return { ...state, ...newState }
            }
        default:
            return state;
    };
}