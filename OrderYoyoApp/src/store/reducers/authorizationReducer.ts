import { initialState } from '../applicationState'
import { AnyAction } from 'redux';
import { KnownActions, LoginActionTypes } from '../actions/loginActions'
import { AuthorizationState } from '../state/authorizationState'
import { CookieType, setCookie } from '../../common/cookie';

export default function authorizationReducer(state: AuthorizationState = initialState.authorization, action: KnownActions): AuthorizationState {
    switch (action.type) {
        case LoginActionTypes.LOGIN_SUCESSFUL:
            if (action.rememberMe) {
                setCookie(CookieType.auth, action.authToken, action.expiryDate)
            }
            return { ...state, ...{ token : action.authToken }
            };
        default:
            return state;
    };
}