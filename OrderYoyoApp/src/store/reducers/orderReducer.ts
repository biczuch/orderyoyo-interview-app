import { initialState } from '../applicationState'
import { AnyAction } from 'redux';
import { KnownActions, OrderActionTypes } from '../actions/orderActions'
import { OrderState } from '../state/orderState';

export default function orderReducer(state: OrderState = initialState.orderState, action: KnownActions): OrderState {
    switch (action.type) {
        case OrderActionTypes.ORDERS_REQUESTED:
            return { ...state, ...{ isLoading: true } };
        case OrderActionTypes.ORDERS_RECEIVED:
            return { ...state, ...{ orders: action.orders, isLoading: false } }
        case OrderActionTypes.ORDER_SELECTED:
            return { ...state, ...{ selectedOrderId: action.orderId } }
        default:
            return state;
    };
}