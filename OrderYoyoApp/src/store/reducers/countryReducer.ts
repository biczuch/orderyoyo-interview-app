import { initialState } from '../applicationState'
import { AnyAction } from 'redux';
import { KnownActions, CountryActionTypes } from '../actions/countryActions'
import { CountryState } from '../state/countryState';

export default function countryReducer(state: CountryState = initialState.countryState, action: KnownActions): CountryState {
    switch (action.type) {
        case CountryActionTypes.COUNTRIES_REQUESTED:
            return { ...state, ...{ isLoading: true } };
        case CountryActionTypes.COUNTRIES_RECEIVED:
            return { ...state, ...{ countries: action.countries, isLoading: false } }
        case CountryActionTypes.COUNTRY_SELECTED:
            return { ...state, ...{ selectedCountryCode: action.selectedCountryCode } }
        default:
            return state;
    };
}