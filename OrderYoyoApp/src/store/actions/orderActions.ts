import { bindActionCreators, ActionCreator, Action, Dispatch } from 'redux'
import { ThunkAction, ThunkDispatch } from 'redux-thunk'
import { ApplicationState } from '../applicationState';
import { securedApi } from '../../api/api';
import { Order } from '../state/orderState';
import { mapOrders } from '../../api/mappers/orderMapper';

export const enum OrderActionTypes {
    ORDERS_REQUESTED = "ORDERS_REQUESTED",
    ORDERS_RECEIVED = "ORDERS_RECEIVED",
    ORDER_SELECTED = "ORDER_SELECTED"
}

interface OrdersRequestedActions extends Action {
    type: OrderActionTypes.ORDERS_REQUESTED
}

interface OrderReceivedAction extends Action {
    type: OrderActionTypes.ORDERS_RECEIVED,
    orders: Array<Order>
}

interface OrderSelectedAction extends Action {
    type: OrderActionTypes.ORDER_SELECTED,
    orderId: number
}

export type KnownActions = OrdersRequestedActions | OrderReceivedAction | OrderSelectedAction

export type OrderThunkAction<R> = ThunkAction<R, ApplicationState, undefined, KnownActions>

export const actionCreators = {
    requestOrders: (countryCode: string): OrderThunkAction<void> => {
        return (dispatch, getState, extraArgument) => {
            const api = securedApi(getState().authorization.token);
            api.getOrders(countryCode).then((response) => {
                const orders: Array<Order> = mapOrders(response);
                dispatch({ type: OrderActionTypes.ORDERS_RECEIVED, orders })
            });

            dispatch({ type: OrderActionTypes.ORDERS_REQUESTED })
        }
    },
    orderDetailsSelected: (orderId:number): OrderThunkAction<void> => {
        return (dispatch, getState, extraArgument) => {
            dispatch({ type: OrderActionTypes.ORDER_SELECTED, orderId: orderId })
        }
    }
}