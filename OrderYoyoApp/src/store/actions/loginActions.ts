import { bindActionCreators, ActionCreator, Action, Dispatch } from 'redux'
import { ThunkAction, ThunkDispatch } from 'redux-thunk'
import { ApplicationState } from '../applicationState';
import { api } from '../../api/api'
import { setCookie, CookieType } from '../../common/cookie';

export const enum LoginActionTypes {
    LOGIN_REQUESTED = "LOGIN_REQUESTED",
    LOGIN_SUCESSFUL = "LOGIN_SUCCESSFUL",
    LOGIN_FAILURE = "LOGIN_FAILURE"
}

interface LoginRequestedAction extends Action {
    type: LoginActionTypes.LOGIN_REQUESTED
}

interface LoginSuccessfulAction extends Action {
    type: LoginActionTypes.LOGIN_SUCESSFUL
    authToken: string
    expiryDate: number,
    rememberMe: boolean
};

interface LoginFailureAction extends Action {
    type: LoginActionTypes.LOGIN_FAILURE,
    errors: Array<string>
}

export type KnownActions = LoginRequestedAction | LoginFailureAction | LoginSuccessfulAction

export type LoginThunkAction<R> = ThunkAction<R, ApplicationState, undefined, KnownActions>

export const actionCreators = {
    requestLogin: (username: string, password: string, rememberMe: boolean): LoginThunkAction<void> => {
        return (dispatch, getState, extraArgument) => {
            api.login(username, password)
                .then(function (response) {
                    dispatch({
                        type: LoginActionTypes.LOGIN_SUCESSFUL,
                        authToken: response.data.access_token,
                        expiryDate: response.data.expires_in,
                        rememberMe: rememberMe
                    })
                })
                .catch(function (error) {
                    const errors = ["Login failed"]
                    dispatch({ type: LoginActionTypes.LOGIN_FAILURE, errors })
                });
                
            dispatch({ type: LoginActionTypes.LOGIN_REQUESTED })
        }
    }
}