import { bindActionCreators, ActionCreator, Action, Dispatch } from 'redux'
import { ThunkAction, ThunkDispatch } from 'redux-thunk'
import { ApplicationState } from '../applicationState';
import { securedApi } from '../../api/api';
import { Country } from '../state/countryState';
import { setCookie, CookieType } from '../../common/cookie';

export const enum CountryActionTypes {
    COUNTRIES_REQUESTED = "COUNTRIES_REQUESTED",
    COUNTRIES_RECEIVED = "RECEIVE_COUNTRIES",
    COUNTRY_SELECTED = "COUNTRY_SELECTED"
}

interface RequestCountriesAction extends Action {
    type: CountryActionTypes.COUNTRIES_REQUESTED
}

interface CountriesReceivedAction extends Action {
    type: CountryActionTypes.COUNTRIES_RECEIVED,
    countries: Array<Country>
}

interface CountrySelectedAction extends Action {
    type: CountryActionTypes.COUNTRY_SELECTED,
    selectedCountryCode: string
}

export type KnownActions = RequestCountriesAction | CountriesReceivedAction | CountrySelectedAction

export type CountryThunkAction<R> = ThunkAction<R, ApplicationState, undefined, KnownActions>

export const actionCreators = {
    requestCountries: (): CountryThunkAction<void> => {
        return (dispatch, getState, extraArgument) => {
            const api = securedApi(getState().authorization.token);
            api.getCountries.then((response) => {

                const countries = response.data.map((x: any) => x as Country)
                dispatch({ type: CountryActionTypes.COUNTRIES_RECEIVED, countries })
            });

            dispatch({ type: CountryActionTypes.COUNTRIES_REQUESTED })
        }
    },
    selectCountry: (countryCode:string): CountryThunkAction<void> => {
        return (dispatch, getState, extraArgument) => {
            setCookie(CookieType.selectedCountryCode, countryCode);
            dispatch({ type: CountryActionTypes.COUNTRY_SELECTED, selectedCountryCode: countryCode })
        }
    }
}