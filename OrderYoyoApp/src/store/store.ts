import { createStore, combineReducers, applyMiddleware } from 'redux'
import countryReducer from './reducers/countryReducer'
import { initialState, ApplicationState } from './applicationState'
import { composeWithDevTools } from 'redux-devtools-extension'
import reduxThunk from 'redux-thunk'
import { formsStateDefaults } from './state/formState';
import formsReducer from './reducers/formReducers';
import { createBrowserHistory } from 'history'
import { connectRouter, routerMiddleware } from 'connected-react-router'
import authorizationReducer from './reducers/authorizationReducer';
import orderReducer from './reducers/orderReducer';

export const history = createBrowserHistory();

let enhancers = composeWithDevTools(
    applyMiddleware(reduxThunk)
);

const rootReducer = combineReducers<ApplicationState>({
    authorization: authorizationReducer,
    formsErrorState: formsReducer,
    countryState: countryReducer,
    orderState: orderReducer
});

export const store = createStore(connectRouter(history)(rootReducer), initialState, enhancers)