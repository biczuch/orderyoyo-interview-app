import { formsStateDefaults, FormsState } from "./state/formState";
import { countryStateDefaults, CountryState } from "./state/countryState";
import { AuthorizationState, authorizationStateDefaults } from "./state/authorizationState";
import { OrderState, orderStateDefaults,} from "./state/orderState";


export interface ApplicationState {
    authorization: AuthorizationState,
    countryState: CountryState,
    formsErrorState: FormsState,
    orderState: OrderState
}

export const initialState: ApplicationState = {
    authorization: authorizationStateDefaults,
    formsErrorState: formsStateDefaults,
    countryState: countryStateDefaults,
    orderState: orderStateDefaults,
}