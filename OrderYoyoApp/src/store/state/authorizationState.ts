import { getCookie, CookieType } from "../../common/cookie";

export interface AuthorizationState {
    token?: string
}

export const authorizationStateDefaults: AuthorizationState = {
    token: getCookie(CookieType.auth),
}
