export interface SimpleFormState {
    hasError: boolean,
    errors: Array<string>,
    isSuccessful: boolean
}

export interface FormsState {
    loginForm: SimpleFormState
}

export const noErrors = (): SimpleFormState => {
    return {
        hasError: false,
        errors: [],
        isSuccessful: false
    }
}

export const formsStateDefaults: FormsState = {
    loginForm: noErrors()
}