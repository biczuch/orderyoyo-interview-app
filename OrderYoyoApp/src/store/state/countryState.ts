import { CookieType, getCookie } from "../../common/cookie";

export interface Country {
    name: string,
    code: string
}

export interface CountryState {
    selectedCountryCode: string,
    countries: Array<Country>,
    isLoading: boolean
}

export const countryStateDefaults: CountryState = {
    selectedCountryCode: getCookie(CookieType.selectedCountryCode) || "",
    countries: [],
    isLoading: true
}
