import { Country } from "./countryState";

export interface Customer {
    name: string,
    phone: string,
    address: string,
    email: string
}

export interface OpenTime {
    openHourId: number,
    openHoursFrom: string,
    openHoursTo: string,
    dayOfWeek: number, //starts from 0
    isClosed: boolean
}

export interface Restaurant {
    restaurantId: number,
    name: string,
    phone?: string,
    email?: string,
    openTime: Array<OpenTime>,
    country: Country,
}

export interface Order {
    orderId: number,
    customerName: Customer,
    created: Date,
    restaurant: Restaurant
    totalAmount: 0,
    currency: string
}

export interface OrderState {
    selectedOrderId: number,
    orders: Array<Order>,
    isLoading: boolean
}

export const orderStateDefaults: OrderState = {
    selectedOrderId: 0,
    orders: [],
    isLoading: true
}
