import * as React from "react";
import { hot } from "react-hot-loader";
import Routes from "./routes/routes";
import { Provider } from "react-redux";
import { store, history } from "./store/store";
import { ConnectedRouter } from "connected-react-router";

import 'antd/dist/antd.css'

class App extends React.Component<{}, {}> {

    constructor(props: any) {
        super(props);
    }

    render() {
        return (
            <Provider store={store}>
                <ConnectedRouter history={history} >
                    <Routes />
                </ConnectedRouter>
            </Provider >
        );
    }
}

export default hot(module)(App);