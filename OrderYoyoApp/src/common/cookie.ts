export const enum CookieType {
    auth = "auth",
    selectedCountryCode = "selectedCountryCode"
}

export const setCookie = (name: CookieType, value: any, expiration?: number) => {
    let cookieExpiration = "";
    if (expiration) {
        let date = new Date();
        date.setTime(date.getTime() + expiration);
        cookieExpiration = "; expires=" + date.toUTCString();
    }
    document.cookie = name + "=" + (value || "") + cookieExpiration + "; path=/"
}

export const getCookie = (cookie: CookieType) => {
    const cookies = document.cookie.split(';')
    const cookieName = cookie.toString();
    for (let i = 0; i < cookies.length; i++) {
        let currentCookie = cookies[i];
        while (currentCookie.charAt(0) == ' ')
            currentCookie = currentCookie.substring(1, currentCookie.length);
        if (currentCookie.indexOf(cookieName) == 0)
            return currentCookie.substring(cookieName.length + 1, currentCookie.length);
    }
    return null;
}

export const eraseCookie = (cookie: CookieType) => {
    document.cookie = cookie.toString() + '=; Max-Age=-99999999;';
}