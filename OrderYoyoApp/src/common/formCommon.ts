import * as yup from 'yup'
import { FormikValues, FormikProps, FormikErrors, FormikTouched } from 'formik';
import { CheckboxChangeEvent } from "antd/lib/checkbox";

export const formItemLayout = {
    labelCol: {
        xs: { span: 24 },
        sm: { span: 8 },
    },
    wrapperCol: {
        xs: { span: 24 },
        sm: { span: 8 },
    },
};

export const tailFormItemLayout = {
    wrapperCol: {
        xs: {
            span: 24,
            offset: 0,
        },
        sm: {
            span: 16,
            offset: 8,
        },
    },
};

export const nameOfField = <T>() => 
    (name: keyof T) => name.toString()

export const formErrorForField = <T extends FormikValues>(errors: FormikErrors<T>, touched: FormikTouched<T>) => {
    return (name: keyof T) => {
        if (touched[name] && errors[name]) {
            return errors[name];
        }
        return null;
    }
}

export const isRequiredForFields = <T extends any>(validationSchema: yup.ObjectSchema<T>, object: (T)) => {
    return (name: keyof T) => {
        let fieldSchema = yup.reach(validationSchema, name.toString(), object);
        const { tests } = fieldSchema.describe()
        const isRequired = tests.some((t: any) => t === "required")
        return isRequired;
    }
}

export enum FormItemType {
    Normal,
    Tail
}

export const hasFormError = <T extends FormikValues>(errors: FormikErrors<T>, touched: FormikTouched<T>) => {
    return (name: keyof T): 'success' | 'warning' | 'error' | 'validating' => {
        if (touched[name] && errors[name]) {
            return "error";
        }
        if (touched[name])
            return "success";

        return null;
    }
}

export const defaultFormItemProps = <T extends any>(formikProps: FormikProps<any>, validationSchema: yup.ObjectSchema<T>) => {
    return (name: keyof (T), formItemType: FormItemType = FormItemType.Normal) => {

        const field = yup.reach(validationSchema, name.toString(), formikProps.values);

        let formItemProps : any = {
            required: isRequiredForFields(validationSchema, formikProps.values)(name.toString()),
            hasFeedback: true,
            validateStatus: hasFormError(formikProps.errors, formikProps.touched)(name.toString()),
            help: formErrorForField(formikProps.errors, formikProps.touched)(name.toString()),
        }

        if (formItemType && formItemType == FormItemType.Tail) {
            formItemProps = { ...tailFormItemLayout, ...formItemProps };
        }
        else if (formItemType == FormItemType.Normal) {
            formItemProps = {
                ...formItemLayout,
                ...formItemProps,
                label: field.describe().label ? field.describe().label : name.toString()
            };
        }

        return formItemProps;
    }
}

export const defaultCheckboxProps = <T extends any>(formikProps: FormikProps<any>, validationSchema: yup.ObjectSchema<T>) => {
    return (name: keyof (T), useValidationSchemeLabel : boolean = true) => {
        let defaultProps : any = {
            name: nameOfField<T>()(name),
            onChange: (val: CheckboxChangeEvent) => {
                formikProps.setFieldValue(name.toString(), val.target.checked)
            },
            checked: formikProps.values[name.toString()],
        }

        if (useValidationSchemeLabel) {
            const validationField = yup.reach(validationSchema, name.toString(), formikProps.values);
            const description = validationField.describe();
            defaultProps = { ...defaultProps, children: description.label ? description.label : name.toString() }
        }

        return defaultProps;
    }
}

export const defaultInputProps = <T extends object, F>(formikProps: FormikProps<any>) => {
    return (name: keyof (F)) => {
        return {
            name: nameOfField<F>()(name),
            value: formikProps.values[name.toString()],
            onChange: formikProps.handleChange,
            onBlur: formikProps.handleBlur,
        }
    }
}

export const FormProps = <T extends any>(formikProps: FormikProps<any>, validationSchema: yup.ObjectSchema<T>) => {
    return {
        formItemProps: defaultFormItemProps<T>(formikProps, validationSchema),
        inputProps: defaultInputProps<any, T>(formikProps),
        checkboxProps: defaultCheckboxProps<T>(formikProps, validationSchema)
    }
}