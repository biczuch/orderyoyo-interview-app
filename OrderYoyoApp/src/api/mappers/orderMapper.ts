import { Order, OpenTime } from "../../store/state/orderState";
import { AxiosResponse } from "axios";
import ordersTableContainer from "../../components/ordersTableContainer";

export const mapOrders = (response: any): Array<Order> => {

    const orders: Array<Order> = response.data.map((x: any): Order => {

        const customer = x.customerName;
        const restaurant = x.restaurant

        const openTimes: Array<OpenTime> = restaurant.openTime.map((ot:any): OpenTime => {
            return {
                openHourId: ot.openHourId,
                openHoursFrom: ot.openHoursFrom,
                openHoursTo: ot.openHoursTo,
                dayOfWeek: ot.dayOfWeek,
                isClosed: ot.isClosed
            }
        });

        return {
            orderId: x.orderId,
            customerName: {
                name: customer.name,
                phone: customer.phone,
                address: customer.address,
                email: customer.email,
            },
            created: new Date(x.created),
            restaurant: {
                restaurantId: x.restaurant.restaurantId,
                name: restaurant.name,
                phone: restaurant.phone,
                email: restaurant.email,
                openTime: openTimes,
                country: {
                    name: restaurant.country.name,
                    code: restaurant.country.code
                }
            },
            totalAmount: x.totalAmount,
            currency: x.currency
        }
    });

    return orders;
}