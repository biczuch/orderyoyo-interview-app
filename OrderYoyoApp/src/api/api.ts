import axios from 'axios'
import * as qs from 'qs'

const root = axios.create({
    baseURL: 'https://iwaitersupportapi.azurewebsites.net',
    timeout: 10000,
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    proxy: {
        host: 'localhost.fiddler',
        port: 8888
    }
})


export const api = {
    login: (username: string, password: string) =>
        root.post('/api/token', qs.stringify({ username: username, password: password }))
}

export const securedApi = (authToken: string) => {

    root.defaults.headers.common['Authorization'] = "Bearer " + authToken;
    
    return {
        getCountries: root.get('/api/countries'),
        getOrders: (countryCode: string) => root.get('/api/order/' + countryCode)

    }
}