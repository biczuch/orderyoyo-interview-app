import { Col, Row, Alert } from 'antd'
import * as React from 'react'
import { SimpleFormState } from '../store/state/formState';

function FormErrors(props: { formErrors: SimpleFormState }) {

    const formErrors = props.formErrors;

    const errorList = () => {
        return (<ul>
            {formErrors.errors.map((val, index) => <li key="index">{val}</li>)}
        </ul>);
    }

    if (formErrors.hasError) {
        return (
            <Row>
                <Col offset={4} span={16}>
                    <Alert type="error" showIcon
                        message={"Error"} description={errorList()} />
                </Col>
            </Row>
        )
    }
    return null;
}

export default FormErrors;